import os
from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse

from . import database
#from .models import PageView

# Create your views here.

def index(request):
    
    return render(request, 'welcome/index.html', {
        'hostname': '',
        'database': '',
        'count': ''
    })

def health(request):
    return HttpResponse(1)