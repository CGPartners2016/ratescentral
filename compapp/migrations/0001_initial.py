# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MortgageRates',
            fields=[
                ('Attribute', models.CharField(max_length=30, primary_key=True, serialize=False)),
                ('Val1', models.CharField(max_length=40)),
                ('Val2', models.CharField(max_length=10)),
                ('Val3', models.CharField(max_length=10)),
                ('Val4', models.CharField(max_length=10)),
                ('Val5', models.CharField(max_length=10)),
                ('Row', models.IntegerField()),
                ('MinVal', models.CharField(max_length=10)),
            ],
            options={
                'managed': True,
            },
        ),
    ]
