import os
from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse

from .models import mortgagerates

def internet(request):
 hostname = os.getenv('HOSTNAME', 'unknown')
 return render(request, 'compapp/Internet.html', {'hostname': hostname})
 
def mobile1(request):
 hostname = os.getenv('HOSTNAME', 'unknown')
 return render(request, 'compapp/MobileBig3.html', {'hostname': hostname})
 
def mobile2(request):
 hostname = os.getenv('HOSTNAME', 'unknown')
 return render(request, 'compapp/MobileDisc.html', {'hostname': hostname}) 

def index(request):
 #mortgageRatesList = MortgageRates.objects.all().order_by('Row')
 mortgageRatesList = mortgagerates.objects.all()
 #print len(mortgageRatesList)
 ratesTable = ''
 
 if mortgageRatesList:
   
  ratesTable = '<table border=0 cellpadding=0 cellspacing=0  style="width:40%" align="center">'
  ratesTable = ratesTable + '<tr><th class=xl68 Colspan="6">CLOSED RATES</th></tr>'	
  ratesTable = ratesTable + '<tr><th class=xl68 Colspan="6">&nbsp;</th></tr>'	
  ratesTable = ratesTable + '<tr><th class=xl68>Term</th>'
  ratesTable = ratesTable + '<th class=xl68 style="border-left:none;"><a href="https://www.tdcanadatrust.com/products-services/banking/mortgages/numbers-resl_d.jsp" target="_blank">TD</a></th>'
  ratesTable = ratesTable + '<th class=xl68 style="border-left:none;"><a href="http://www.rbcroyalbank.com/mortgages/mortgage-rates.html" target="_blank">RBC</a></th>'
  ratesTable = ratesTable + '<th class=xl68 style="border-left:none;"><a href="http://www.scotiabank.com/ca/en/0,,1112,00.html" target="_blank">SCOTIA</a></th>'	 
  ratesTable = ratesTable + '<th class=xl68 style="border-left:none;"><a href="https://www.cibc.com/ca/rates/index.html" target="_blank">CIBC</a></th>'
  ratesTable = ratesTable + '<th class=xl68 style="border-left:none;"><a href="http://www.bmo.com/main/personal/mortgages/mortgage-rates" target="_blank">BMO</a></th></tr>'	
  
  for rates in mortgageRatesList:
   
   if rates.Attribute != 'Last Update:':  
    ratesTable = ratesTable + '<tr class=xl65 align="center"><td>' + rates.Attribute + '</td>' 
    
    if rates.Val1 == rates.MinVal:   
     ratesTable = ratesTable + '<td class=xl65 style="border-left:none;" align="center">&nbsp;<b><font color="green">' + rates.Val1 + '</b></font>&nbsp;</td>'
    else:
     ratesTable = ratesTable + '<td class=xl65 style="border-left:none;" align="center">&nbsp;' + rates.Val1 + '&nbsp;</td>'
    
    if rates.Val2 == rates.MinVal:   
     ratesTable = ratesTable + '<td class=xl65 style="border-left:none;" align="center">&nbsp;<b><font color="green">' + rates.Val2 + '</b></font>&nbsp;</td>'
    else:
     ratesTable = ratesTable + '<td class=xl65 style="border-left:none;" align="center">&nbsp;' + rates.Val2 + '&nbsp;</td>'
     
    if rates.Val3 == rates.MinVal:   
     ratesTable = ratesTable + '<td class=xl65 style="border-left:none;" align="center">&nbsp;<b><font color="green">' + rates.Val3 + '</b></font>&nbsp;</td>'
    else:
     ratesTable = ratesTable + '<td class=xl65 style="border-left:none;" align="center">&nbsp;' + rates.Val3 + '&nbsp;</td>'
    
    if rates.Val4 == rates.MinVal:   
     ratesTable = ratesTable + '<td class=xl65 style="border-left:none;" align="center">&nbsp;<b><font color="green">' + rates.Val4 + '</b></font>&nbsp;</td>'
    else:
     ratesTable = ratesTable + '<td class=xl65 style="border-left:none;" align="center">&nbsp;' + rates.Val4 + '&nbsp;</td>'
    
    if rates.Val5 == rates.MinVal:   
     ratesTable = ratesTable + '<td class=xl65 style="border-left:none;" align="center">&nbsp;<b><font color="green">' + rates.Val5 + '</b></font>&nbsp;</td>'
    else:
     ratesTable = ratesTable + '<td class=xl65 style="border-left:none;" align="center">&nbsp;' + rates.Val5 + '&nbsp;</td>'    
    
    ratesTable = ratesTable + '</tr>' 
   else:
    ratesTable = ratesTable + '<tr><td colspan="6" class=xl65 align="center">&nbsp;</td></tr>'
    ratesTable = ratesTable + '<tr><td class=xl65 align="center"><b>' + rates.Attribute + '</b></td>'
    ratesTable = ratesTable + '<td class=xl65 style="border-left:none;" align="center" colspan="5">' + rates.Val1 + '</td></tr>'     
         
  ratesTable = ratesTable + '</table>'
 else:
  ratesTable = '<p>Rates not available. Contact Support Email.</p>'
  
 return render(request, 'compapp/index.html', {'ratesTable': ratesTable})