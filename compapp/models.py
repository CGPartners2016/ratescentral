from django.db import models

# Create your models here.
class mortgagerates(models.Model):
 Attribute = models.CharField(max_length=30, primary_key=True)
 Val1 = models.CharField(max_length=40)
 Val2 = models.CharField(max_length=10)
 Val3 = models.CharField(max_length=10)
 Val4 = models.CharField(max_length=10)
 Val5 = models.CharField(max_length=10)
 Row = models.IntegerField()
 MinVal = models.CharField(max_length=10)
 class Meta:
  db_table = 'mortgagerates'
  managed=True
   
def __unicode__(self):
 return self.name