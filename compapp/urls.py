from django.conf.urls import include, url
from django.contrib import admin

#from welcome.views import index, health

from django.views.generic import TemplateView
from compapp.views import index, internet, mobile1,mobile2

from . import views

urlpatterns = [
    #url(r'^$', views.rates, name='rates'),
    url(r'^$', index),
    url(r'^Internet', internet),
    url(r'^Mobile1', mobile1),
    url(r'^Mobile2', mobile2),
]

