from django.conf.urls import include, url
from django.contrib import admin

from compapp.views import index
from welcome.views import health

urlpatterns = [
    # Examples:
    # url(r'^$', 'project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', index),
    url(r'^health$', health),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^compapp/',include('compapp.urls')),
]
